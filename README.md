# Starter Front

## Dependencies
  * npm

## Installing
```
npm install
```

### Dependencies installed
	* autoprefixer
	* cssnano
	* gulp
	* gulp-postcss
	* gulp-sass
	* gulp-size
	* gulp-util
	* lost
	* postcss-font-magician
	* postcss-import
	* reset-css
	* rucksack-css
	* run-sequence